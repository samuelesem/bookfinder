import 'dart:convert';

import 'package:book_finder/interceptors/book_api_interceptor.dart';
import 'package:book_finder/models/book_detail_model.dart';
import 'package:book_finder/models/book_response.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/http.dart';

class BookService {

  String apiUrl = 'https://api.itbook.store/1.0/';
  http.Client client = InterceptedClient.build(interceptors: [
    BookInterceptor(),
  ], retryPolicy: RetryBookInterceptor(),);

  Future<BookResponse> fetchBooks(String searchTerm, String page) async {
      final response =
          await client.get(Uri.parse(apiUrl + 'search/' + searchTerm + '/' + page));
      if (response.statusCode == 200) {
        String data = response.body;
        BookResponse responseObject =
            BookResponse.fromJson(json.decode(data));
        return responseObject;
      } else {
        throw Exception('Failed to get data');
      }
  }

  Future<BookDetailModel> fetchBookDetail(String isbn) async {
    final response = await http.get(Uri.parse(apiUrl + 'books/' + isbn));
    if (response.statusCode == 200) {
      String data = response.body;
      BookDetailModel responseObject =
          BookDetailModel.fromJson(json.decode(data));
      return responseObject;
    } else {
      throw Exception('Failed to get data');
    }
  }

}
