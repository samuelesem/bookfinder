import 'package:json_annotation/json_annotation.dart';
import 'package:book_finder/models/book_model.dart';

part 'book_response.g.dart';

@JsonSerializable()
class BookResponse {
  String total;
  String page;
  List<BookModel> books;

  BookResponse(this.total, this.page, this.books);


  factory BookResponse.fromJson(Map<String,dynamic> data) => _$BookResponseFromJson(data);

  Map<String,dynamic> toJson() => _$BookResponseToJson(this);

  @override
  String toString() {
    return 'BookResponse{total: $total, page: $page, books: $books}';
  }
}
