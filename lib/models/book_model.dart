import 'package:json_annotation/json_annotation.dart';
part 'book_model.g.dart';

@JsonSerializable()
class BookModel {
  String title;
  String subtitle;
  String isbn13;
  String price;
  String image;
  String url;

  BookModel(
      this.title, this.subtitle, this.isbn13, this.price, this.image, this.url);

  factory BookModel.fromJson(Map<String,dynamic> data) => _$BookModelFromJson(data);

  Map<String,dynamic> toJson() => _$BookModelToJson(this);

  @override
  String toString() {
    return 'BookModel{title: $title, subtitle: $subtitle, isbn13: $isbn13, price: $price, image: $image, url: $url}';
  }
}

