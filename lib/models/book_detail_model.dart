import 'package:json_annotation/json_annotation.dart';
part 'book_detail_model.g.dart';

@JsonSerializable()
class BookDetailModel {
  String error;
  String title;
  String subtitle;
  String authors;
  String publisher;
  String isbn10;
  String isbn13;
  String pages;
  String year;
  String rating;
  String desc;
  String price;
  String image;
  String url;
  Map<String, dynamic>? pdf;

  BookDetailModel(
      this.error,
      this.title,
      this.subtitle,
      this.authors,
      this.publisher,
      this.isbn10,
      this.isbn13,
      this.pages,
      this.year,
      this.rating,
      this.desc,
      this.price,
      this.image,
      this.url,
      this.pdf);


  factory BookDetailModel.fromJson(Map<String,dynamic> data) => _$BookDetailModelFromJson(data);

  Map<String,dynamic> toJson() => _$BookDetailModelToJson(this);

}
