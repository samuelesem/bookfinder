import 'package:flutter/material.dart';

class SimpleTextInput extends StatefulWidget {
  final String hintText;
  final Function(String) onSearch;
  final MaterialAccentColor? materialAccentColor;
  final TextEditingController controllerValue;
  final TextInputType? keyboardType;

  SimpleTextInput(
      {required this.hintText,
      required this.onSearch,
      this.materialAccentColor,
      required this.controllerValue,
      this.keyboardType});

  @override
  _SimpleTextInputState createState() => _SimpleTextInputState();
}

class _SimpleTextInputState extends State<SimpleTextInput> {
  bool _validate = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 8.0, bottom: 10.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.25),
                        spreadRadius: 0,
                        blurRadius: 4,
                        offset: Offset(0, 4), // changes position of shadow
                      ),
                    ],
                  ),
                  child: TextFormField(
                      controller: widget.controllerValue,
                      style: TextStyle(color: Color.fromRGBO(0, 75, 138, 1)),
                      decoration: new InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: new OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 1,
                                color: Colors.red,
                                style: BorderStyle.none),
                            borderRadius: new BorderRadius.circular(
                              15.0,
                            )),
                        focusedBorder: new OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 1,
                                color: Colors.red,
                                style: BorderStyle.none),
                            borderRadius: new BorderRadius.circular(
                              15.0,
                            )),
                        hintText: widget.hintText,
                        contentPadding: EdgeInsets.all(15),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.search),
                          onPressed: () {
                            if (widget.controllerValue.text.isEmpty) {
                              setState(() {
                                _validate = false;
                              });
                            } else {
                              setState(() {
                                _validate = true;
                              });
                            }
                            widget.onSearch(widget.controllerValue.text);
                          },
                        ),
                      ),
                      keyboardType: widget.keyboardType),
                ),
              ),
            ],
          ),
        ),
        !_validate
            ? Text(
                'Value cannot be empty',
                style: TextStyle(color: Colors.red),
              )
            : Container(),
      ],
    );
  }
}
