import 'package:book_finder/models/book_detail_model.dart';
import 'package:book_finder/services/book_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BookDetailPage extends StatefulWidget {
  final String isbn;

  BookDetailPage({Key? key, required this.isbn}) : super(key: key);

  @override
  _BookDetailPageState createState() => _BookDetailPageState();
}

class _BookDetailPageState extends State<BookDetailPage> {
  BookDetailModel? bookDetailModel;
  BookService _bookService = BookService();

  @override
  void initState() {
    _bookService.fetchBookDetail(widget.isbn).then((value) => setState(() {
          bookDetailModel = value;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(
          'Book detail',
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: (bookDetailModel != null)
              ? Container(
                  margin:
                      EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 20),
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black12,
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          offset: const Offset(
                            5.0,
                            5.0,
                          ),
                          blurRadius: 10.0,
                          spreadRadius: 2.0,
                        ),
                        BoxShadow(
                          color: Colors.white,
                          offset: const Offset(0.0, 0.0),
                          blurRadius: 0.0,
                          spreadRadius: 0.0,
                        )
                      ],
                      borderRadius:
                          BorderRadius.all(const Radius.circular(30))),
                  child: Column(children: [
                    if (bookDetailModel?.image != null)
                      Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              bottomLeft: Radius.circular(15)),
                          child: Image.network(
                            bookDetailModel!.image,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    if (bookDetailModel!.title != null)
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        color: Color.fromRGBO(244, 244, 244, 1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Column(children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: Text(
                                    bookDetailModel!.title,
                                    style: const TextStyle(fontSize: 22),
                                    maxLines: 2,
                                    textAlign: TextAlign.center,
                                    softWrap: true,
                                    overflow: TextOverflow.fade,
                                  ),
                                ),
                                if (bookDetailModel!.subtitle != null)
                                  Container(
                                    margin: EdgeInsets.only(bottom: 20),
                                    child: Text(
                                      bookDetailModel!.subtitle,
                                      style: const TextStyle(fontSize: 20),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                                if (bookDetailModel!.authors != null)
                                  Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    child: Text(
                                      bookDetailModel!.authors,
                                      style: const TextStyle(
                                          fontSize: 18, color: Colors.black87),
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                                if (bookDetailModel!.year != null)
                                  Container(
                                    margin: EdgeInsets.only(bottom: 20),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    child: Text(
                                      bookDetailModel!.year,
                                      style: const TextStyle(
                                          fontSize: 18,
                                          color: Colors.blue,
                                          fontStyle: FontStyle.italic),
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                              ]),
                            ),
                          ],
                        ),
                      ),
                    if (bookDetailModel!.desc != null)
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        margin: EdgeInsets.only(top: 20, bottom: 20),
                        child: Text(
                          bookDetailModel!.desc,
                          style: const TextStyle(fontSize: 16),
                          textAlign: TextAlign.start,
                          softWrap: true,
                        ),
                      ),
                    if (bookDetailModel!.url != null)
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: ElevatedButton(
                            onPressed: () {
                              Clipboard.setData(
                                  ClipboardData(text: bookDetailModel!.url));
                            },
                            child: const Text('Copy link',
                                style: TextStyle(fontSize: 20)),
                            style: raisedButtonStyle),
                      )
                  ]),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(child: Text('Loading')),
                  ],
                ),
        ),
      ),
    );
  }

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.white,
    primary: Colors.blue,
    elevation: 10,
    minimumSize: Size(88, 36),
    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(20)),
    ),
  );
}
