import 'package:book_finder/models/book_model.dart';
import 'package:book_finder/models/book_response.dart';
import 'package:book_finder/models/page_state_enum.dart';
import 'package:book_finder/screens/book_detail_page.dart';
import 'package:book_finder/services/book_service.dart';
import 'package:book_finder/widgets/simple_text_input.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  SearchPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  BookService _bookService = BookService();
  BookResponse? bookResponse;
  List<BookModel> books = [];
  Status status = Status.none;
  final TextEditingController searchController = TextEditingController();
  int page = 1;
  int total = 0;
  String searchTerm = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SimpleTextInput(
                hintText: 'Please type a term',
                controllerValue: searchController,
                onSearch: (String value) {
                  page = 1;
                  fetchBooks(value, page);
                  searchTerm = value;
                },
              ),
              (books != null)
                  ? ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: books.length,
                      itemBuilder: (context, index) {
                        BookModel book = books[index];
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BookDetailPage(
                                        isbn: book.isbn13,
                                      )),
                            );
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            elevation: 5,
                            child: Row(
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      bottomLeft: Radius.circular(15)),
                                  child: Image.network(
                                    book.image,
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          book.title,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                        Text(
                                          book.subtitle,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    )
                  : (status == Status.loading)
                      ? Center(child: Text('Loading'))
                      : Container(),
              SizedBox(
                height: 20,
              ),

            ],
          ),
        ),
      ),
    );
  }

  fetchBooks(String value, int page) {
    setState(() {
      status = Status.loading;
    });
    if (value.length > 0) {
      _bookService.fetchBooks(value, page.toString()).then((value) => {
            setState(() {
              bookResponse = value;
              if (value != null &&
                  value.books != null &&
                  value.books.length > 0) {
                if (page == 1) {
                  total = int.parse(value.total);
                  books = value.books;
                  books.toSet().toList();
                } else {
                  print(value.books.length);
                  books.addAll(value.books);
                  books.toSet().toList();
                }
                status = Status.loaded;
              }
              if (bookResponse != null && (books.length) != total) {
                print(page);
                fetchBooks(searchTerm, page + 1);
              }
            })
          });
    } else {
      books=[];
    }
  }

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.white,
    primary: Colors.blue,
    elevation: 10,
    minimumSize: Size(88, 36),
    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(20)),
    ),
  );
}
